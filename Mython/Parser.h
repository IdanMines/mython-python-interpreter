#pragma once
#include "InterpreterException.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include "Type.h"
#include "Helper.h"
#include "Integer.h"
#include "Boolean.h"
#include "String.h"
#include "Void.h"
#include "List.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>


class Parser
{
public:
	static Type* parseString(std::string str);
	static Type* getType(const std::string& string);
	static bool isLegalVarName(const std::string varName);
	static bool makeAssignment(const std::string str);
	static Type* getVariableValue(const std::string str);
	static Type* getLen(const std::string str);
	static void deleteVariables();
	static void deleteVariable(const std::string str);
	static std::string getTypeName(const std::string str);
	static std::unordered_map<std::string, Type* > _variables;
private:
	static bool isList(const std::string string);

};
