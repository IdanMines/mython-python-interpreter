#pragma once
#include "Sequence.h"
#include "String.h"
#include "Integer.h"
#include "Boolean.h"
#include "SyntaxException.h"
#include <vector>
class List : public Sequence
{
public:
	List(const std::string);
	bool isPrintable() const;
	std::string toString() const;

private:
	Type* getType(const std::string str);
	std::vector<Type*> _list;

};
