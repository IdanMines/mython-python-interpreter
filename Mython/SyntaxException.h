#pragma once
#include "InterpreterException.h"


class SyntaxException : public InterpreterException
{
public:
	virtual const char* what() const noexcept;
};




class NameErrorException : public InterpreterException
{
public:
	NameErrorException(const std::string name);
	virtual const char* what() const noexcept;
private:

	std::string _name;

};