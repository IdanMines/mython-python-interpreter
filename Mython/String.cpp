#include "String.h"

String::String(std::string value)
{
	this->_value = value.substr(1, value.length() - 2);

}

bool String::isPrintable() const
{
	return true;
}
std::string String::toString() const
{
	if(this->_value.find("'") == std::string::npos)
		return "'" + this->_value + "'";
	else 
		return '"' + this->_value + '"';

}