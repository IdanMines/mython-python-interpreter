#include "Parser.h"
#include <iostream>
std::unordered_map<std::string, Type*> Parser::_variables;


Type* Parser::parseString(std::string str)
{
	if (str.length() > 0)
	{
		if (str[0] == ' ' || str[0] == '\t')
			throw IndentationException();
		Helper::rtrim(str);



		if (makeAssignment(str))
		{
			Type* returnType = new Void();
			returnType->setIsTemp(true);
			return returnType;

		}

		if (str.substr(0, 5) == "type(")
		{
			Type* varValue = nullptr;

			if(str.substr(5, 4) == "len(")
			{
				varValue = getLen(str.substr(5,str.length()-6));
			}
			else
			{
				int str_len = str.length();
				std::string var_name_or_value = str.substr(5, str_len - 5 - 1);
				varValue = (isLegalVarName(var_name_or_value) && getVariableValue(var_name_or_value)) ? getVariableValue(var_name_or_value) : getType(var_name_or_value);
			}


			if (varValue)
			{
				std::string typeName = getTypeName(varValue->toString());
				std::cout << "<type '" << typeName << "'>" << std::endl;
				return new Void();
			}
		}
		if (str.substr(0, 3) == "del")
		{
			if (isLegalVarName(str.substr(4, str.length() - 4)))
			{

				if(getVariableValue(str.substr(4, str.length() - 4)))
				{
					deleteVariable(str.substr(4, str.length() - 4));
					return new Void();
				}
			}
		}
		if (str.substr(0, 4) == "len(")
		{
			if(getLen(str))
				return getLen(str);
			else
				return new Void();
			


		}


		if (isLegalVarName(str))
		{
			Type* varValue = getVariableValue(str);
			if (varValue)
			{
				return varValue;
			}
		}


		Type* type = getType(str);
		if (type != NULL)
			return type;
		else
			throw SyntaxException();
		std::cout << "sss" << std::endl;



	}

	return nullptr;
}
Type* Parser::getLen(const std::string str)
{
	int str_len = str.length();
	std::string var_name_or_value = str.substr(4, str_len - 5);
	Type* varValue =(isLegalVarName(var_name_or_value) && getVariableValue(var_name_or_value)) ? getVariableValue(var_name_or_value) : getType(var_name_or_value);
	if (varValue)
	{
		std::string typeName = getTypeName(varValue->toString());
		if (typeName == "List")
		{
			int len = 0;
			std::string listString = varValue->toString();
			listString = listString.substr(1, listString.length() - 1);
			if(listString != "")
			{
				len = 1;
				for (int i = 0; i < listString.length(); i++)
				{
					if (listString[i] == ',')
						len++;
				}
			}

			Type* type = new Integer(len);
			return type;

		}
		else if (typeName == "String")
		{
			Type* type = new Integer(varValue->toString().length() - 2);
			return type;

		}
		else
		{
			std::cout << "TypeError: object of type �" << typeName << "� has no len() ." << std::endl;
		}
	}
	return nullptr;

}

std::string Parser::getTypeName(const std::string string)
{
	if (Helper::isBoolean(string))
	{
		return "Boolean";
	}
	if (Helper::isString(string))
	{
		return "String";


	}
	if (Helper::isInteger(string))
	{
		return "Integer";


	}
	if (isList(string))
	{
		return "List";
	}
	return "None";


}
Type* Parser::getType(const std::string& string)
{
	if (Helper::isBoolean(string))
	{
		bool value = (string == "True") ? true : false;
		Type* type = new Boolean(value);
		type->setIsTemp(true);
		return type;
	}
	if (Helper::isString(string))
	{

		Type* type = new String(string);
		type->setIsTemp(true);
		return type;

	}
	if(Helper::isInteger(string))
	{
		std::string nString = string;
		Helper::removeLeadingZeros(nString);
		int stringToInt = std::stoi(nString);
		Type* type = new Integer(stringToInt);
		type->setIsTemp(true);
		return type;

	}
	if(isList(string))
	{
		Type* type = new List(string);
		type->setIsTemp(true);
		return type;

	}
	return NULL;
}
bool Parser::isList(const std::string str)
{
	int vars = 0;
	int pos;
	if (str[0] != '[' || str[str.length() - 1] != ']')
		return false;
	std::string copy = str.substr(1, str.length() - 2);
	for (int i = 0; i < str.length(); i++)
	{
		if (str[i] == ',')
			vars++;
	}
	if (vars == 0)
	{
		Helper::trim(copy);
		if (copy == "")
			return true;
		Type* var = getType(copy);
		if (var == NULL && copy != "")
			return false;
	}
	while ((pos = copy.find(',')) != std::string::npos)
	{
		std::string varString = copy.substr(0, pos);
		Helper::trim(varString);
		copy = copy.substr(pos + 1, copy.length() - 1);
		Type* var = getType(varString);
		if (var == NULL)
		{ 
			return false;
		}

	}
	if(vars!= 0)
	{
		Helper::trim(copy);
		Type* var = getType(copy);
		if (var == NULL)
		{
			return false;
		}

	}
	return true;
}

bool Parser::isLegalVarName(const std::string varName)
{
	if(Helper::isDigit(varName[0]))
	{
		return false;
	}
	for (int i = 0; i < varName.length(); i++)
	{
		if (!(Helper::isDigit(varName[i]) || Helper::isLetter(varName[i]) || Helper::isUnderscore(varName[i])))
		{
			return false;
		}
	}
	return true;

}

bool Parser::makeAssignment(const std::string str)
{
	int equalsSignOccurrences = 0;
	int equalsSignPosition = 0;
	if (str[0] == '=' || str[str.length() - 1] == '=')
		return false;

	for (int i = 1; i < str.length() - 1; i++)
	{
		if (str[i] == '=')
		{
			equalsSignOccurrences++;
			equalsSignPosition = i;
		}
	}
	if (equalsSignOccurrences != 1)
		return false;
	std::string varName = str.substr(0, equalsSignPosition);
	Helper::trim(varName);
	std::string varValue = str.substr(equalsSignPosition + 1, str.length() - equalsSignOccurrences - 1);
	Helper::trim(varValue);
	if (!isLegalVarName(varName))
	{
		throw NameErrorException(varName);
		return false;
	}

	Type* value = getType(varValue);
	if(value == NULL)
	{
		//check if the variable value is a variable itself (variable = other_variable)
		if(isLegalVarName(varValue))
		{
			//check if the variable exist if not throw name exception
			//if the variable exist deep copy it to the new variable
			Type* copyVarValue = getVariableValue(varValue);
			if (copyVarValue != nullptr)
			{
				std::string copyVarStringValue = copyVarValue->toString();
				value = getType(copyVarStringValue);

			}


		}
		else
		{
			throw SyntaxException();
			return false;
		}
	}
	value->setIsTemp(false);
	if(_variables.find(varName) != _variables.end())
	{
		_variables[varName] = value;
		return true;
	}
	_variables.insert(std::make_pair(varName, value));
	return true;

}

Type* Parser::getVariableValue(const std::string str)
{
	if (_variables.find(str) == _variables.end())
	{
		//std::cout << "NameError : name '" << str << "' is not defined" << std::endl;
		throw NameErrorException(str);
		return nullptr;
	}

	return _variables[str];
}

void Parser::deleteVariables()
{
	for (auto i = _variables.begin(); i != _variables.end(); i++)
	{
		delete i->second;
	}
	_variables.clear();
}
void Parser::deleteVariable(const std::string str)
{
	Type* type = _variables[str];
	_variables.erase(str);
	delete type;



}
