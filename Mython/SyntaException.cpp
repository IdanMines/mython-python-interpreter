#include "SyntaxException.h"


const char* SyntaxException::what() const noexcept
{
	return "SyntaxError: invalid syntax";
}

NameErrorException::NameErrorException(const std::string name)
{
	this->_name = name;

}
const char* NameErrorException::what() const noexcept
{
	std::string response = "NameError : name '" + _name + "' is not defined";
	char* char_array = new char[response.length() + 1];

	strcpy_s(char_array, response.length() + 1, response.c_str());
	return char_array;

}


