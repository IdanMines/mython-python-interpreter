#pragma once
#include <string>
#include "Helper.h"

class Type
{
public:
	void setIsTemp(bool isTemp);
	bool getIsTemp() const ;
	virtual bool isPrintable() const = 0;
	virtual std::string toString() const = 0;

protected:
	bool _isTemp = false;

};
