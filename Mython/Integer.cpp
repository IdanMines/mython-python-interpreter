#include "Integer.h"

Integer::Integer(int value)
{
	this->_value = value;
}
bool Integer::isPrintable() const
{ 
	return true;
}
std::string Integer::toString() const 
{
	std::string value = std::to_string(_value);
	Helper::removeLeadingZeros(value);
	return value;
}
