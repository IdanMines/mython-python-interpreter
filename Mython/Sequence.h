#pragma once
#include "Type.h"

class Sequence : public Type
{
public:
	bool isPrintable() const = 0;
	std::string toString() const = 0;
};
