#pragma once
#include "List.h"
#include <iostream>

List::List(const std::string str)
{
	int vars = 0;
	int pos;
	std::string copy = str.substr(1,str.length()-2);
	for (int i = 0; i < str.length(); i++)
	{
		if (str[i] == ',')
			vars++;
	}
	if(vars == 0)
	{
		Helper::trim(copy);
		if (copy != "")
		{
			Type* var = List::getType(copy);
			this->_list.push_back(var);

		}


	}
	while((pos = copy.find(','))!= std::string::npos)
	{
		std::string varString = copy.substr(0, pos);
		Helper::trim(varString);
		copy = copy.substr(pos+1, copy.length() - 1);
		Type* var = List::getType(varString);
		if (var == nullptr)
			throw NameErrorException(varString);
		else
		{
			this->_list.push_back(var);
			std::cout << "var str: " << varString << std::endl;
		}
	}
	if(vars != 0)
	{
		Type* var = List::getType(copy);
		if (var == nullptr)
			throw NameErrorException(copy);
		else
		{
			this->_list.push_back(var);
			std::cout << "var str: " << copy << std::endl;
		}
	}



}

Type* List::getType(const std::string string)
{
	if (Helper::isBoolean(string))
	{
		bool value = (string == "True") ? true : false;
		Type* type = new Boolean(value);
		return type;
	}
	if (Helper::isString(string))
	{

		Type* type = new String(string);
		return type;

	}
	if (Helper::isInteger(string))
	{
		std::string nString = string;
		Helper::removeLeadingZeros(nString);
		int stringToInt = std::stoi(nString);
		Type* type = new Integer(stringToInt);
		return type;

	}
	return nullptr;
}
bool List::isPrintable() const { return true; }
std::string List::toString() const
{
	std::string response = "[";
	for (auto i = _list.begin(); i != _list.end(); i++)
	{
		response += (*i)->toString();
		response += ", ";
	}
	response = response.substr(0, response.length() - 2);
	response += "]";
	return response;
}