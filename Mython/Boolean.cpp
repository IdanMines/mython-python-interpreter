#include "Boolean.h"

Boolean::Boolean(bool value)
{
	this->_value = value;
}
bool Boolean::isPrintable() const
{
	return true;
}
std::string Boolean::toString() const
{
	std::string stringValue = (this->_value) ? "True" : "False";
	return stringValue;

}

